/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dropDownList;

/**
 *
 * @author William Sanchez
 */
public class ListItem {

    private String text;
    private String value;
    private boolean selected;
    private boolean enabled;

    public ListItem() {
        this.text = "";
        this.value = "";
        this.selected = false;
        this.enabled = false;
    }

    public ListItem(String text, String value, boolean selected, boolean enabled) {
        this.text = text;
        this.value = value;
        this.selected = selected;
        this.enabled = enabled;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
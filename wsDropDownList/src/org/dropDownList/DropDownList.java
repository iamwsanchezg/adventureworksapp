/*
 * DropDownList.java
 * @author : William Sánchez
 * Agregados los métodos setSelectedValue y getSelectedValue
 * Created on 30 de marzo de 2019, 10:00 AM
 */

package org.dropDownList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

public final class DropDownList extends JComboBox {
    
    private final static long serialVersionUID=87563L;
    private String dataValueField;
    private String dataTextField;
    private Connection connection;
    private String selectedValue;
    private int selectedIndex;
    private ListItem selectedItem;
    private final List dataValueList=new ArrayList();
    
    public DropDownList() {
        initComponents();
    }
    
    public DropDownList(Statement st, String sql)throws SQLException {
        ResultSet rs=st.executeQuery(sql);
        while(rs.next()){
            addItem(rs.getObject(1));
        }
        rs.close();
    }
    
    @SuppressWarnings({"unchecked", "UnusedAssignment"})
    public DropDownList(Connection con, String sqlStatement,String dataTextField,String dataValueField)throws SQLException {
        Statement st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
        ResultSet rs=st.executeQuery(sqlStatement);
        while(rs.next()){
            dataValueList.add(rs.getString(dataValueField));
            addItem(rs.getString(dataTextField));
        }
        rs.close();rs=null;
        st.close();st=null;
       setSelectedIndex(-1);
    }
   @SuppressWarnings("unchecked")
   public DropDownList(Connection con, String sql,String dataTextField,String dataValueField,
        JTable table,TableColumn stickyColumn)throws SQLException {
        Statement st=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
        ResultSet rs=st.executeQuery(sql);
        this.dataValueField=dataValueField;
        while(rs.next()){
            dataValueList.add(rs.getString(dataValueField));
            addItem(rs.getString(dataTextField));
        }
       stickToColumn(table,stickyColumn);
       setSelectedIndex(-1);//Para que aparezca sin selecci�n
    }
    public List getDataValueList(){
        return dataValueList;
    }
    
   /* Este metodo es llamado cuando se envia un objeto statement, una cadena de ejecucion
     *sql y el nombre de la columna que queremos obtener
     **/
    public DropDownList(Statement st, String sql,String dataTextField)throws SQLException {
        this.dataTextField = dataTextField;
        this.dataValueField ="";
        ResultSet rs=st.executeQuery(sql);
        new DropDownList(rs,dataTextField);
    }
    /* Este metodo es llamado cuando se realiza un ResultSet rs y se desea obtener datos 
     colocarlos en un combo este campo es un index int del resultSet.
     */
    public DropDownList(ResultSet rs, int index)throws SQLException {
        this.dataTextField = "";
        this.dataValueField ="";     
        while(rs.next()){
            addItem(rs.getObject(index));
        }
        rs.close();
    }
    
  public DropDownList(ResultSet rs, String dataTextField)throws SQLException {
        this.dataTextField = dataTextField;
        this.dataValueField ="";
        while(rs.next()){
            addItem(rs.getObject(dataTextField));
        }
        rs.close();
    }
  
    public String getDataValueField() {
        return dataValueField;
    }

    public void setDataValueField(String dataValueField) {
        this.dataValueField = dataValueField;
    }

    public String getDataTextField() {
        return dataTextField;
    }

    public void setDataTextField(String dataTextField) {
        this.dataTextField = dataTextField;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
  //Devuelve el valor de la dataValueList seg�n el indice seleccionado
   public String getSelectedValue(){
       try{
           if (getSelectedIndex()>=0){
                selectedValue = ((String)getDataValueList().get(getSelectedIndex())).trim();
           }
       }
       catch(java.lang.ArrayIndexOutOfBoundsException e){
                      return null;
       }
       return selectedValue;
    }
   //Establece o selecciona un elemento de la lista pasando como parametro
   //un elemento de la lista oculta
    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
        setSelectedIndex(dataValueList.indexOf(selectedValue));
    }
   
//    public int getSelectedIndex() {
//        return selectedIndex;
//    }
//
//    public void setSelectedIndex(int selectedIndex) {
//        this.selectedIndex = selectedIndex;
//    }
//
//    public ListItem getSelectedItem() {
//        return selectedItem;
//    }

    public void setSelectedItem(ListItem selectedItem) {
        this.selectedItem = selectedItem;
    }

  //Establece el DropDownList como editor de una columna
    public void stickToColumn(JTable table,
                                 TableColumn stickyColumn) {
        //Prepara el editor para la colunmna indicada.
        stickyColumn.setCellEditor(new DefaultCellEditor(this));

        //ToolTipText para la columna indicada.
        DefaultTableCellRenderer dtcr =
                new DefaultTableCellRenderer();
        dtcr.setToolTipText("Click para mostrar opciones");
        stickyColumn.setCellRenderer(dtcr);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
    }// </editor-fold>//GEN-END:initComponents
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    
}

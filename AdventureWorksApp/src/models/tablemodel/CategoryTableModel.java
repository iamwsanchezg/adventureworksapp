/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package models.tablemodel;

import java.lang.reflect.Field;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import models.production.Category;

/**
 *
 * @author William Sanchez
 */
public class CategoryTableModel extends AbstractTableModel{
    private Object columnNames[];
    private Object rowData[][];
    
    private void initColumnames(){
        Field fields[] = Category.class.getDeclaredFields();
        columnNames = new String[fields.length];
        int c=0;
        for (Field field:fields){
            columnNames[c]=field.getName();
            c++;
        }
    }
    public CategoryTableModel(){
        initColumnames();
    }
    public CategoryTableModel(Category[] categories){
        initColumnames();
        rowData = new Object[categories.length][columnNames.length];
        int c=0;
        for(Category category:categories){
            rowData[c]=new Object[]{
                                    category.getProductCategoryID(),
                                    category.getName(),
                                    category.getRowguid(),
                                    category.getModifiedDate()
                                };
            c++;
        }
        setDataModel(rowData);
    }
    public void setDataModel(Object[][] data){
         rowData = data;
    }
    public TableModel getModel(){
      TableModel model = new DefaultTableModel(
            rowData,
            columnNames
        );
        return model;
    }
    
    @Override
    public int getRowCount() {
        return rowData.length;
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex];
    }
    public void saveTableData(){
        
    }
}

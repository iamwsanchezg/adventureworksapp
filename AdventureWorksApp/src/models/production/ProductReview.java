/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package models.production;

public class ProductReview {
  private Long productreviewid;
  private Long productid;
  private String reviewername;
  private java.sql.Timestamp reviewdate;
  private String emailaddress;
  private Long rating;
  private String comments;
  private java.sql.Timestamp modifieddate;

  public Long getProductreviewid() {
    return productreviewid;
  }

  public void setProductreviewid(Long productreviewid) {
    this.productreviewid = productreviewid;
  }

  public Long getProductid() {
    return productid;
  }

  public void setProductid(Long productid) {
    this.productid = productid;
  }

  public String getReviewername() {
    return reviewername;
  }

  public void setReviewername(String reviewername) {
    this.reviewername = reviewername;
  }

  public java.sql.Timestamp getReviewdate() {
    return reviewdate;
  }

  public void setReviewdate(java.sql.Timestamp reviewdate) {
    this.reviewdate = reviewdate;
  }

  public String getEmailaddress() {
    return emailaddress;
  }

  public void setEmailaddress(String emailaddress) {
    this.emailaddress = emailaddress;
  }

  public Long getRating() {
    return rating;
  }

  public void setRating(Long rating) {
    this.rating = rating;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

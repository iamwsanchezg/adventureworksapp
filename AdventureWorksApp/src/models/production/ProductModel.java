/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package models.production;

public class ProductModel {
  private Long productmodelid;
  private String name;
  private String catalogdescription;
  private String instructions;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getProductmodelid() {
    return productmodelid;
  }

  public void setProductmodelid(Long productmodelid) {
    this.productmodelid = productmodelid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCatalogdescription() {
    return catalogdescription;
  }

  public void setCatalogdescription(String catalogdescription) {
    this.catalogdescription = catalogdescription;
  }

  public String getInstructions() {
    return instructions;
  }

  public void setInstructions(String instructions) {
    this.instructions = instructions;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

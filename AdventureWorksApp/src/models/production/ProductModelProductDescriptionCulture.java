/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package models.production;

public class ProductModelProductDescriptionCulture {
  private Long productmodelid;
  private Long productdescriptionid;
  private String cultureid;
  private java.sql.Timestamp modifieddate;

  public Long getProductmodelid() {
    return productmodelid;
  }

  public void setProductmodelid(Long productmodelid) {
    this.productmodelid = productmodelid;
  }

  public Long getProductdescriptionid() {
    return productdescriptionid;
  }

  public void setProductdescriptionid(Long productdescriptionid) {
    this.productdescriptionid = productdescriptionid;
  }

  public String getCultureid() {
    return cultureid;
  }

  public void setCultureid(String cultureid) {
    this.cultureid = cultureid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

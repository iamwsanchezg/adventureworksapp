/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package models.production;

public class ProductPhoto {
  private Long productphotoid;
  private String thumbnailphoto;
  private String thumbnailphotofilename;
  private String largephoto;
  private String largephotofilename;
  private java.sql.Timestamp modifieddate;

  public Long getProductphotoid() {
    return productphotoid;
  }

  public void setProductphotoid(Long productphotoid) {
    this.productphotoid = productphotoid;
  }

  public String getThumbnailphoto() {
    return thumbnailphoto;
  }

  public void setThumbnailphoto(String thumbnailphoto) {
    this.thumbnailphoto = thumbnailphoto;
  }

  public String getThumbnailphotofilename() {
    return thumbnailphotofilename;
  }

  public void setThumbnailphotofilename(String thumbnailphotofilename) {
    this.thumbnailphotofilename = thumbnailphotofilename;
  }

  public String getLargephoto() {
    return largephoto;
  }

  public void setLargephoto(String largephoto) {
    this.largephoto = largephoto;
  }

  public String getLargephotofilename() {
    return largephotofilename;
  }

  public void setLargephotofilename(String largephotofilename) {
    this.largephotofilename = largephotofilename;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

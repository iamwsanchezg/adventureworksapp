/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package models.production;

/**
 *
 * @author William Sanchez
 */
public class Product {
  private Long productId;
  private String name;
  private String productNumber;
  private String makeFlag;
  private String finishedGoodsFlag;
  private String color;
  private Long safetyStockLevel;
  private Long reorderPoint;
  private String standardCost;
  private String listPrice;
  private String size;
  private String sizeUnitMeasureCode;
  private String weightUnitMeasureCode;
  private Double weight;
  private Long daysToManufacture;
  private String productLine;
  private String _class;
  private String style;
  private Long productSubcategoryId;
  private Long productModelId;
  private java.sql.Timestamp sellStartDate;
  private java.sql.Timestamp sellEndDate;
  private java.sql.Timestamp discontinuedDate;
  private String rowguid;
  private java.sql.Timestamp modifiedDate;

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productid) {
    this.productId = productid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getProductNumber() {
    return productNumber;
  }

  public void setProductNumber(String productnumber) {
    this.productNumber = productnumber;
  }

  public String getMakeflag() {
    return makeFlag;
  }

  public void setMakeflag(String makeflag) {
    this.makeFlag = makeflag;
  }

  public String getFinishedGoodsFlag() {
    return finishedGoodsFlag;
  }

  public void setFinishedGoodsFlag(String finishedGoodsFlag) {
    this.finishedGoodsFlag = finishedGoodsFlag;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public Long getSafetyStockLevel() {
    return safetyStockLevel;
  }

  public void setSafetyStockLevel(Long safetyStockLevel) {
    this.safetyStockLevel = safetyStockLevel;
  }

  public Long getReorderPoint() {
    return reorderPoint;
  }

  public void setReorderPoint(Long reorderPoint) {
    this.reorderPoint = reorderPoint;
  }

  public String getStandardCost() {
    return standardCost;
  }

  public void setStandardCost(String standardCost) {
    this.standardCost = standardCost;
  }

  public String getListPrice() {
    return listPrice;
  }

  public void setListPrice(String listPrice) {
    this.listPrice = listPrice;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getSizeUnitMeasureCode() {
    return sizeUnitMeasureCode;
  }

  public void setSizeUnitMeasureCode(String sizeUnitMeasureCode) {
    this.sizeUnitMeasureCode = sizeUnitMeasureCode;
  }

  public String getWeightUnitMeasureCode() {
    return weightUnitMeasureCode;
  }

  public void setWeightUnitMeasureCode(String weightUnitMeasureCode) {
    this.weightUnitMeasureCode = weightUnitMeasureCode;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public Long getDaysToManufacture() {
    return daysToManufacture;
  }

  public void setDaysToManufacture(Long daysToManufacture) {
    this.daysToManufacture = daysToManufacture;
  }

  public String getProductLine() {
    return productLine;
  }

  public void setProductLine(String productLine) {
    this.productLine = productLine;
  }

  public String getClass_() {
    return _class;
  }

  public void setClass_(String _class) {
    this._class = _class;
  }

  public String getStyle() {
    return style;
  }

  public void setStyle(String style) {
    this.style = style;
  }

  public Long getProductSubcategoryId() {
    return productSubcategoryId;
  }

  public void setProductSubcategoryId(Long productSubcategoryId) {
    this.productSubcategoryId = productSubcategoryId;
  }

  public Long getProductModelId() {
    return productModelId;
  }

  public void setProductModelId(Long productModelId) {
    this.productModelId = productModelId;
  }

  public java.sql.Timestamp getSellStartDate() {
    return sellStartDate;
  }

  public void setSellStartDate(java.sql.Timestamp sellStartDate) {
    this.sellStartDate = sellStartDate;
  }

  public java.sql.Timestamp getSellEndDate() {
    return sellEndDate;
  }

  public void setSellEndDate(java.sql.Timestamp sellEndDate) {
    this.sellEndDate = sellEndDate;
  }

  public java.sql.Timestamp getDiscontinuedDate() {
    return discontinuedDate;
  }

  public void setDiscontinuedDate(java.sql.Timestamp discontinuedDate) {
    this.discontinuedDate = discontinuedDate;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(java.sql.Timestamp modifiedDate) {
    this.modifiedDate = modifiedDate;
  }
}

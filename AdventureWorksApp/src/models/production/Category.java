/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package models.production;

import java.util.Date;

/**
 *
 * @author William Sanchez
 */
public class Category {
    private int ProductCategoryID;
    private String Name;
    private String rowguid; 
    private Date ModifiedDate;

    public int getProductCategoryID() {
        return ProductCategoryID;
    }

    public void setProductCategoryID(int ProductCategoryID) {
        this.ProductCategoryID = ProductCategoryID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getRowguid() {
        return rowguid;
    }

    public void setRowguid(String rowguid) {
        this.rowguid = rowguid;
    }

    public Date getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(Date ModifiedDate) {
        this.ModifiedDate = ModifiedDate;
    }

    public Category(int ProductCategoryID, String Name, String rowguid, Date ModifiedDate) {
        this.ProductCategoryID = ProductCategoryID;
        this.Name = Name;
        this.rowguid = rowguid;
        this.ModifiedDate = ModifiedDate;
    }

    public Category(String Name) {
        this.Name = Name;
    }

    public Category() {
    }
    
}

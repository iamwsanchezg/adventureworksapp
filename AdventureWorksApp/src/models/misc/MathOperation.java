/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package models.misc;
import misc.Math;
/**
 *
 * @author William Sánchez
 */
public class MathOperation {
    protected Double term1;
    protected Double term2;
    protected String operator;
    public MathOperation(){
        term1=0d;
        term2=0d;
        operator="";
    }
    public void setTerm1(Double t1){
        term1=t1;
    }
    public void setTerm1(String t1){
        term1=Double.valueOf(t1);
    }    
    public Double getTerm1(){
        return term1;
    }
    public String getTerm1ToString(){
        return Math.getNumberToString(term1);
    }  
    public void setTerm2(Double t2){
        term2=t2;
    }
    public void setTerm2(String t2){
        term2=Double.valueOf(t2);
    } 
    public Double getTerm2(){
        return term2;
    }   
    public String getTerm2ToString(){
        return Math.getNumberToString(term2);
    }     
    public void setOperator(String op){
        operator=op;
    }
    public String getOperator(){
        return operator;
    }
    public Double resolve(){        
        Double r;
        switch(operator){
            case "+":
                r= term1+term2;
                break;
            case "-":
                r= term1-term2;
                break;
            case "x":
                r= term1*term2;
                break;
            case "/":
                r= term1/term2;
                break;   
            case "n!":
                r = Math.getFactorial(term1.intValue());
                break;
            case ".nf.":
                r = Math.getFibonacci(term1.intValue());
                break;
            default:
                r=0d;
                break;
        }
        return r;
    }
     public String resolveToString(){        
        Double r;
        String sr;
        switch(operator){
            case "+":
                r= term1+term2;
                sr = Math.getNumberToString(r);
                break;
            case "-":
                r= term1-term2;
                sr = Math.getNumberToString(r);
                break;
            case "x":
                r= term1*term2;
                sr = Math.getNumberToString(r);
                break;
            case "/":
                r= term1/term2;
                sr = Math.getNumberToString(r);
                break;   
            case "n!":
                r = Math.getFactorial(term1.intValue());
                sr = Math.getNumberToString(r);
                break;
            case ".nf.":
                r = Math.getFibonacci(term1.intValue());
                sr = Math.getNumberToString(r);
                break;
            case "n/x?":
                sr = Math.isPrimeNumber(term1.intValue())?"Es primo":"No es primo";
                break;                
            case "0112..":
                sr = Math.getFibonacciSeries(term1.intValue());
                break;            
            default:
                r=0d;
                sr = Math.getNumberToString(r);
                break;
        }
        return sr;
    }   
    @Override
    public String toString(){
        String r;
        switch(operator){
            case "n!":
                r="fact("+String.valueOf(term1.intValue())+")";
                break;
            case ".nf.":
                r="fib("+String.valueOf(term1.intValue())+")";
                break;
            case "n/x?":
                r="isPrime("+String.valueOf(term1.intValue())+")";
                break;
            default:
                r=Math.getNumberToString(term1)+operator+Math.getNumberToString(term2)+"=";
            break;                
        }
       return r;
    }    
}

package models.sales;

public class Customer {
  private Long customerid;
  private Long personid;
  private Long storeid;
  private Long territoryid;
  private String accountnumber;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getCustomerid() {
    return customerid;
  }

  public void setCustomerid(Long customerid) {
    this.customerid = customerid;
  }

  public Long getPersonid() {
    return personid;
  }

  public void setPersonid(Long personid) {
    this.personid = personid;
  }

  public Long getStoreid() {
    return storeid;
  }

  public void setStoreid(Long storeid) {
    this.storeid = storeid;
  }

  public Long getTerritoryid() {
    return territoryid;
  }

  public void setTerritoryid(Long territoryid) {
    this.territoryid = territoryid;
  }

  public String getAccountnumber() {
    return accountnumber;
  }

  public void setAccountnumber(String accountnumber) {
    this.accountnumber = accountnumber;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

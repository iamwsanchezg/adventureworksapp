package models.sales;

public class SalesTerritoryHistory {
  private Long businessentityid;
  private Long territoryid;
  private java.sql.Timestamp startdate;
  private java.sql.Timestamp enddate;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getBusinessentityid() {
    return businessentityid;
  }

  public void setBusinessentityid(Long businessentityid) {
    this.businessentityid = businessentityid;
  }

  public Long getTerritoryid() {
    return territoryid;
  }

  public void setTerritoryid(Long territoryid) {
    this.territoryid = territoryid;
  }

  public java.sql.Timestamp getStartdate() {
    return startdate;
  }

  public void setStartdate(java.sql.Timestamp startdate) {
    this.startdate = startdate;
  }

  public java.sql.Timestamp getEnddate() {
    return enddate;
  }

  public void setEnddate(java.sql.Timestamp enddate) {
    this.enddate = enddate;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

package models.sales;

public class CountryRegionCurrency {
  private String countryregioncode;
  private String currencycode;
  private java.sql.Timestamp modifieddate;

  public String getCountryregioncode() {
    return countryregioncode;
  }

  public void setCountryregioncode(String countryregioncode) {
    this.countryregioncode = countryregioncode;
  }

  public String getCurrencycode() {
    return currencycode;
  }

  public void setCurrencycode(String currencycode) {
    this.currencycode = currencycode;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

package models.sales;

public class SpecialOffer {
  private Long specialofferid;
  private String description;
  private String discountpct;
  private String type;
  private String category;
  private java.sql.Timestamp startdate;
  private java.sql.Timestamp enddate;
  private Long minqty;
  private Long maxqty;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getSpecialofferid() {
    return specialofferid;
  }

  public void setSpecialofferid(Long specialofferid) {
    this.specialofferid = specialofferid;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDiscountpct() {
    return discountpct;
  }

  public void setDiscountpct(String discountpct) {
    this.discountpct = discountpct;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public java.sql.Timestamp getStartdate() {
    return startdate;
  }

  public void setStartdate(java.sql.Timestamp startdate) {
    this.startdate = startdate;
  }

  public java.sql.Timestamp getEnddate() {
    return enddate;
  }

  public void setEnddate(java.sql.Timestamp enddate) {
    this.enddate = enddate;
  }

  public Long getMinqty() {
    return minqty;
  }

  public void setMinqty(Long minqty) {
    this.minqty = minqty;
  }

  public Long getMaxqty() {
    return maxqty;
  }

  public void setMaxqty(Long maxqty) {
    this.maxqty = maxqty;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

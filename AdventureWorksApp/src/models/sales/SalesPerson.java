package models.sales;

public class SalesPerson {
  private Long businessentityid;
  private Long territoryid;
  private String salesquota;
  private String bonus;
  private String commissionpct;
  private String salesytd;
  private String saleslastyear;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getBusinessentityid() {
    return businessentityid;
  }

  public void setBusinessentityid(Long businessentityid) {
    this.businessentityid = businessentityid;
  }

  public Long getTerritoryid() {
    return territoryid;
  }

  public void setTerritoryid(Long territoryid) {
    this.territoryid = territoryid;
  }

  public String getSalesquota() {
    return salesquota;
  }

  public void setSalesquota(String salesquota) {
    this.salesquota = salesquota;
  }

  public String getBonus() {
    return bonus;
  }

  public void setBonus(String bonus) {
    this.bonus = bonus;
  }

  public String getCommissionpct() {
    return commissionpct;
  }

  public void setCommissionpct(String commissionpct) {
    this.commissionpct = commissionpct;
  }

  public String getSalesytd() {
    return salesytd;
  }

  public void setSalesytd(String salesytd) {
    this.salesytd = salesytd;
  }

  public String getSaleslastyear() {
    return saleslastyear;
  }

  public void setSaleslastyear(String saleslastyear) {
    this.saleslastyear = saleslastyear;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

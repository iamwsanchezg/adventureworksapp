package models.sales;

public class CreditCard {
  private Long creditcardid;
  private String cardtype;
  private String cardnumber;
  private Long expmonth;
  private Long expyear;
  private java.sql.Timestamp modifieddate;

  public Long getCreditcardid() {
    return creditcardid;
  }

  public void setCreditcardid(Long creditcardid) {
    this.creditcardid = creditcardid;
  }

  public String getCardtype() {
    return cardtype;
  }

  public void setCardtype(String cardtype) {
    this.cardtype = cardtype;
  }

  public String getCardnumber() {
    return cardnumber;
  }

  public void setCardnumber(String cardnumber) {
    this.cardnumber = cardnumber;
  }

  public Long getExpmonth() {
    return expmonth;
  }

  public void setExpmonth(Long expmonth) {
    this.expmonth = expmonth;
  }

  public Long getExpyear() {
    return expyear;
  }

  public void setExpyear(Long expyear) {
    this.expyear = expyear;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

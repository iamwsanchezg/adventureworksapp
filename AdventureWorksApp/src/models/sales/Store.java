package models.sales;

public class Store {
  private Long businessentityid;
  private String name;
  private Long salespersonid;
  private String demographics;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getBusinessentityid() {
    return businessentityid;
  }

  public void setBusinessentityid(Long businessentityid) {
    this.businessentityid = businessentityid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getSalespersonid() {
    return salespersonid;
  }

  public void setSalespersonid(Long salespersonid) {
    this.salespersonid = salespersonid;
  }

  public String getDemographics() {
    return demographics;
  }

  public void setDemographics(String demographics) {
    this.demographics = demographics;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

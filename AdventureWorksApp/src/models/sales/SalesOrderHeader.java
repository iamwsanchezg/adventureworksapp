package models.sales;

public class SalesOrderHeader {
  private Long salesorderid;
  private Long revisionnumber;
  private java.sql.Timestamp orderdate;
  private java.sql.Timestamp duedate;
  private java.sql.Timestamp shipdate;
  private Long status;
  private String onlineorderflag;
  private String salesordernumber;
  private String purchaseordernumber;
  private String accountnumber;
  private Long customerid;
  private Long salespersonid;
  private Long territoryid;
  private Long billtoaddressid;
  private Long shiptoaddressid;
  private Long shipmethodid;
  private Long creditcardid;
  private String creditcardapprovalcode;
  private Long currencyrateid;
  private String subtotal;
  private String taxamt;
  private String freight;
  private String totaldue;
  private String comment;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getSalesorderid() {
    return salesorderid;
  }

  public void setSalesorderid(Long salesorderid) {
    this.salesorderid = salesorderid;
  }

  public Long getRevisionnumber() {
    return revisionnumber;
  }

  public void setRevisionnumber(Long revisionnumber) {
    this.revisionnumber = revisionnumber;
  }

  public java.sql.Timestamp getOrderdate() {
    return orderdate;
  }

  public void setOrderdate(java.sql.Timestamp orderdate) {
    this.orderdate = orderdate;
  }

  public java.sql.Timestamp getDuedate() {
    return duedate;
  }

  public void setDuedate(java.sql.Timestamp duedate) {
    this.duedate = duedate;
  }

  public java.sql.Timestamp getShipdate() {
    return shipdate;
  }

  public void setShipdate(java.sql.Timestamp shipdate) {
    this.shipdate = shipdate;
  }

  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  public String getOnlineorderflag() {
    return onlineorderflag;
  }

  public void setOnlineorderflag(String onlineorderflag) {
    this.onlineorderflag = onlineorderflag;
  }

  public String getSalesordernumber() {
    return salesordernumber;
  }

  public void setSalesordernumber(String salesordernumber) {
    this.salesordernumber = salesordernumber;
  }

  public String getPurchaseordernumber() {
    return purchaseordernumber;
  }

  public void setPurchaseordernumber(String purchaseordernumber) {
    this.purchaseordernumber = purchaseordernumber;
  }

  public String getAccountnumber() {
    return accountnumber;
  }

  public void setAccountnumber(String accountnumber) {
    this.accountnumber = accountnumber;
  }

  public Long getCustomerid() {
    return customerid;
  }

  public void setCustomerid(Long customerid) {
    this.customerid = customerid;
  }

  public Long getSalespersonid() {
    return salespersonid;
  }

  public void setSalespersonid(Long salespersonid) {
    this.salespersonid = salespersonid;
  }

  public Long getTerritoryid() {
    return territoryid;
  }

  public void setTerritoryid(Long territoryid) {
    this.territoryid = territoryid;
  }

  public Long getBilltoaddressid() {
    return billtoaddressid;
  }

  public void setBilltoaddressid(Long billtoaddressid) {
    this.billtoaddressid = billtoaddressid;
  }

  public Long getShiptoaddressid() {
    return shiptoaddressid;
  }

  public void setShiptoaddressid(Long shiptoaddressid) {
    this.shiptoaddressid = shiptoaddressid;
  }

  public Long getShipmethodid() {
    return shipmethodid;
  }

  public void setShipmethodid(Long shipmethodid) {
    this.shipmethodid = shipmethodid;
  }

  public Long getCreditcardid() {
    return creditcardid;
  }

  public void setCreditcardid(Long creditcardid) {
    this.creditcardid = creditcardid;
  }

  public String getCreditcardapprovalcode() {
    return creditcardapprovalcode;
  }

  public void setCreditcardapprovalcode(String creditcardapprovalcode) {
    this.creditcardapprovalcode = creditcardapprovalcode;
  }

  public Long getCurrencyrateid() {
    return currencyrateid;
  }

  public void setCurrencyrateid(Long currencyrateid) {
    this.currencyrateid = currencyrateid;
  }

  public String getSubtotal() {
    return subtotal;
  }

  public void setSubtotal(String subtotal) {
    this.subtotal = subtotal;
  }

  public String getTaxamt() {
    return taxamt;
  }

  public void setTaxamt(String taxamt) {
    this.taxamt = taxamt;
  }

  public String getFreight() {
    return freight;
  }

  public void setFreight(String freight) {
    this.freight = freight;
  }

  public String getTotaldue() {
    return totaldue;
  }

  public void setTotaldue(String totaldue) {
    this.totaldue = totaldue;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

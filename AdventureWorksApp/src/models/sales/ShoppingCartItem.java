package models.sales;

public class ShoppingCartItem {
  private Long shoppingcartitemid;
  private String shoppingcartid;
  private Long quantity;
  private Long productid;
  private java.sql.Timestamp datecreated;
  private java.sql.Timestamp modifieddate;

  public Long getShoppingcartitemid() {
    return shoppingcartitemid;
  }

  public void setShoppingcartitemid(Long shoppingcartitemid) {
    this.shoppingcartitemid = shoppingcartitemid;
  }

  public String getShoppingcartid() {
    return shoppingcartid;
  }

  public void setShoppingcartid(String shoppingcartid) {
    this.shoppingcartid = shoppingcartid;
  }

  public Long getQuantity() {
    return quantity;
  }

  public void setQuantity(Long quantity) {
    this.quantity = quantity;
  }

  public Long getProductid() {
    return productid;
  }

  public void setProductid(Long productid) {
    this.productid = productid;
  }

  public java.sql.Timestamp getDatecreated() {
    return datecreated;
  }

  public void setDatecreated(java.sql.Timestamp datecreated) {
    this.datecreated = datecreated;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

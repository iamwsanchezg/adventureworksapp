package models.sales;

public class PersonCreditCard {
  private Long businessentityid;
  private Long creditcardid;
  private java.sql.Timestamp modifieddate;

  public Long getBusinessentityid() {
    return businessentityid;
  }

  public void setBusinessentityid(Long businessentityid) {
    this.businessentityid = businessentityid;
  }

  public Long getCreditcardid() {
    return creditcardid;
  }

  public void setCreditcardid(Long creditcardid) {
    this.creditcardid = creditcardid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

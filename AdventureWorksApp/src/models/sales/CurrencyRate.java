package models.sales;

public class CurrencyRate {
  private Long currencyrateid;
  private java.sql.Timestamp currencyratedate;
  private String fromcurrencycode;
  private String tocurrencycode;
  private String averagerate;
  private String endofdayrate;
  private java.sql.Timestamp modifieddate;

  public Long getCurrencyrateid() {
    return currencyrateid;
  }

  public void setCurrencyrateid(Long currencyrateid) {
    this.currencyrateid = currencyrateid;
  }

  public java.sql.Timestamp getCurrencyratedate() {
    return currencyratedate;
  }

  public void setCurrencyratedate(java.sql.Timestamp currencyratedate) {
    this.currencyratedate = currencyratedate;
  }

  public String getFromcurrencycode() {
    return fromcurrencycode;
  }

  public void setFromcurrencycode(String fromcurrencycode) {
    this.fromcurrencycode = fromcurrencycode;
  }

  public String getTocurrencycode() {
    return tocurrencycode;
  }

  public void setTocurrencycode(String tocurrencycode) {
    this.tocurrencycode = tocurrencycode;
  }

  public String getAveragerate() {
    return averagerate;
  }

  public void setAveragerate(String averagerate) {
    this.averagerate = averagerate;
  }

  public String getEndofdayrate() {
    return endofdayrate;
  }

  public void setEndofdayrate(String endofdayrate) {
    this.endofdayrate = endofdayrate;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

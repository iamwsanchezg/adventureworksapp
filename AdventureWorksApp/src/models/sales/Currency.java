package models.sales;

public class Currency {
  private String currencycode;
  private String name;
  private java.sql.Timestamp modifieddate;

  public String getCurrencycode() {
    return currencycode;
  }

  public void setCurrencycode(String currencycode) {
    this.currencycode = currencycode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

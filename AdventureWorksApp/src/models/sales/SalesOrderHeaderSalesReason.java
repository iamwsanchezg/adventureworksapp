package models.sales;

public class SalesOrderHeaderSalesReason {
  private Long salesorderid;
  private Long salesreasonid;
  private java.sql.Timestamp modifieddate;

  public Long getSalesorderid() {
    return salesorderid;
  }

  public void setSalesorderid(Long salesorderid) {
    this.salesorderid = salesorderid;
  }

  public Long getSalesreasonid() {
    return salesreasonid;
  }

  public void setSalesreasonid(Long salesreasonid) {
    this.salesreasonid = salesreasonid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

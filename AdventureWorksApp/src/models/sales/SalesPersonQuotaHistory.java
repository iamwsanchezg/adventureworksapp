package models.sales;

public class SalesPersonQuotaHistory {
  private Long businessentityid;
  private java.sql.Timestamp quotadate;
  private String salesquota;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getBusinessentityid() {
    return businessentityid;
  }

  public void setBusinessentityid(Long businessentityid) {
    this.businessentityid = businessentityid;
  }

  public java.sql.Timestamp getQuotadate() {
    return quotadate;
  }

  public void setQuotadate(java.sql.Timestamp quotadate) {
    this.quotadate = quotadate;
  }

  public String getSalesquota() {
    return salesquota;
  }

  public void setSalesquota(String salesquota) {
    this.salesquota = salesquota;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

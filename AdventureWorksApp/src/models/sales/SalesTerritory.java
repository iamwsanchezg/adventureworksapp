package models.sales;

public class SalesTerritory {
  private Long territoryid;
  private String name;
  private String countryregioncode;
  private String group;
  private String salesytd;
  private String saleslastyear;
  private String costytd;
  private String costlastyear;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getTerritoryid() {
    return territoryid;
  }

  public void setTerritoryid(Long territoryid) {
    this.territoryid = territoryid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCountryregioncode() {
    return countryregioncode;
  }

  public void setCountryregioncode(String countryregioncode) {
    this.countryregioncode = countryregioncode;
  }

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public String getSalesytd() {
    return salesytd;
  }

  public void setSalesytd(String salesytd) {
    this.salesytd = salesytd;
  }

  public String getSaleslastyear() {
    return saleslastyear;
  }

  public void setSaleslastyear(String saleslastyear) {
    this.saleslastyear = saleslastyear;
  }

  public String getCostytd() {
    return costytd;
  }

  public void setCostytd(String costytd) {
    this.costytd = costytd;
  }

  public String getCostlastyear() {
    return costlastyear;
  }

  public void setCostlastyear(String costlastyear) {
    this.costlastyear = costlastyear;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

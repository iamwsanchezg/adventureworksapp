package models.sales;

public class SpecialOfferProduct {
  private Long specialofferid;
  private Long productid;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getSpecialofferid() {
    return specialofferid;
  }

  public void setSpecialofferid(Long specialofferid) {
    this.specialofferid = specialofferid;
  }

  public Long getProductid() {
    return productid;
  }

  public void setProductid(Long productid) {
    this.productid = productid;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

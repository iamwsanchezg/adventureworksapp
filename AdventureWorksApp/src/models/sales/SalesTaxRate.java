package models.sales;

public class SalesTaxRate {
  private Long salestaxrateid;
  private Long stateprovinceid;
  private Long taxtype;
  private String taxrate;
  private String name;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getSalestaxrateid() {
    return salestaxrateid;
  }

  public void setSalestaxrateid(Long salestaxrateid) {
    this.salestaxrateid = salestaxrateid;
  }

  public Long getStateprovinceid() {
    return stateprovinceid;
  }

  public void setStateprovinceid(Long stateprovinceid) {
    this.stateprovinceid = stateprovinceid;
  }

  public Long getTaxtype() {
    return taxtype;
  }

  public void setTaxtype(Long taxtype) {
    this.taxtype = taxtype;
  }

  public String getTaxrate() {
    return taxrate;
  }

  public void setTaxrate(String taxrate) {
    this.taxrate = taxrate;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

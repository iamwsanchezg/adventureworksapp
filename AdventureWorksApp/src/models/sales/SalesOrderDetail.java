package models.sales;

public class SalesOrderDetail {
  private Long salesorderid;
  private Long salesorderdetailid;
  private String carriertrackingnumber;
  private Long orderqty;
  private Long productid;
  private Long specialofferid;
  private String unitprice;
  private String unitpricediscount;
  private String linetotal;
  private String rowguid;
  private java.sql.Timestamp modifieddate;

  public Long getSalesorderid() {
    return salesorderid;
  }

  public void setSalesorderid(Long salesorderid) {
    this.salesorderid = salesorderid;
  }

  public Long getSalesorderdetailid() {
    return salesorderdetailid;
  }

  public void setSalesorderdetailid(Long salesorderdetailid) {
    this.salesorderdetailid = salesorderdetailid;
  }

  public String getCarriertrackingnumber() {
    return carriertrackingnumber;
  }

  public void setCarriertrackingnumber(String carriertrackingnumber) {
    this.carriertrackingnumber = carriertrackingnumber;
  }

  public Long getOrderqty() {
    return orderqty;
  }

  public void setOrderqty(Long orderqty) {
    this.orderqty = orderqty;
  }

  public Long getProductid() {
    return productid;
  }

  public void setProductid(Long productid) {
    this.productid = productid;
  }

  public Long getSpecialofferid() {
    return specialofferid;
  }

  public void setSpecialofferid(Long specialofferid) {
    this.specialofferid = specialofferid;
  }

  public String getUnitprice() {
    return unitprice;
  }

  public void setUnitprice(String unitprice) {
    this.unitprice = unitprice;
  }

  public String getUnitpricediscount() {
    return unitpricediscount;
  }

  public void setUnitpricediscount(String unitpricediscount) {
    this.unitpricediscount = unitpricediscount;
  }

  public String getLinetotal() {
    return linetotal;
  }

  public void setLinetotal(String linetotal) {
    this.linetotal = linetotal;
  }

  public String getRowguid() {
    return rowguid;
  }

  public void setRowguid(String rowguid) {
    this.rowguid = rowguid;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

package models.sales;

public class SalesReason {
  private Long salesreasonid;
  private String name;
  private String reasontype;
  private java.sql.Timestamp modifieddate;

  public Long getSalesreasonid() {
    return salesreasonid;
  }

  public void setSalesreasonid(Long salesreasonid) {
    this.salesreasonid = salesreasonid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getReasontype() {
    return reasontype;
  }

  public void setReasontype(String reasontype) {
    this.reasontype = reasontype;
  }

  public java.sql.Timestamp getModifieddate() {
    return modifieddate;
  }

  public void setModifieddate(java.sql.Timestamp modifieddate) {
    this.modifieddate = modifieddate;
  }
}

/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers.misc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import models.misc.Person;
import views.misc.PersonForm;

/**
 *
 * @author William Sanchez
 */
public class PersonController implements ActionListener {
    PersonForm personFrame;
    JFileChooser d;
    Person person;
    public PersonController(PersonForm f){
        super();
        personFrame = f;
        d = new JFileChooser();
        person = new Person();
    }
   
    public void setPerson(Person b){
        person = b;
    }
    public void setPerson(String filePath){
        File f = new File(filePath);
        readPerson(f);
    }
    public Person getPerson(){
        return person;
    }    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "save":
                d.showSaveDialog(personFrame); 
                person = personFrame.getPersonData();
                writePerson(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(personFrame);  
                person = readPerson(d.getSelectedFile());
                personFrame.setPersonData(person);
                break;
            case "clear":
                personFrame.clear();
                break;

        }        
    }
    private void writePerson(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getPerson());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(PersonController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    private Person readPerson(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Person)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(personFrame, e.getMessage(),personFrame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(PersonController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private Person[] readPersonList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Person[] persons;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
            persons = (Person[]) s.readObject();
        }
        return persons;
    }
}

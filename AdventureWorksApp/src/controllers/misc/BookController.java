/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers.misc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import static javax.swing.JFileChooser.DIRECTORIES_ONLY;
import javax.swing.JOptionPane;
import models.misc.Book;
import models.tablemodel.BookTableModel;
import models.tablemodel.FileTableModel;
import views.misc.BookForm;
import views.misc.BookList;
import views.misc.BookManagerForm;
/**
 *
 * @author William Sanchez
 */
public class BookController implements ActionListener {
    BookForm bookFrame;
    BookList bookList;
    BookManagerForm bookManager;
    JFileChooser d;
    Book book;
    public BookController(BookForm f){
        super();
        bookFrame = f;
        d = new JFileChooser();
        book = new Book();
    }
     public BookController(BookList l){
        super();
        bookList = l;
        d = new JFileChooser();
        book = new Book();
    }   
     public BookController(BookManagerForm m){
        super();
        bookManager = m;
        d = new JFileChooser();
        book = new Book();
    }      
    public void setBook(Book b){
        book = b;
    }
    public void setBook(String filePath){
        File f = new File(filePath);
        readBook(f);
    }
    public Book getBook(){
        return book;
    }    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "save":
                d.showSaveDialog(bookFrame); 
                book = bookFrame.getBookData();
                writeBook(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(bookFrame);  
                book = readBook(d.getSelectedFile());
                bookFrame.setBookData(book);
                break;
            case "clear":
                bookFrame.clear();
                break;
            case "directory":
                d.setFileSelectionMode(DIRECTORIES_ONLY);
                d.showOpenDialog(bookList);
                File f = d.getSelectedFile();
                bookList.setDirectory(f.getPath()); 
                bookList.setBookTableModel(new FileTableModel(f).getModel());
                break;
            case "new":
                showBookForm("");
                break;
            case "show":
                showBookForm(bookList.getSelectedBookFile());
                break;
            case "bookList":
                d.showOpenDialog(bookList);
                File bl = d.getSelectedFile();
                bookManager.setBookListFile(bl.getPath());                
                {
                    try {
                        bookManager.setBookTableModel(new BookTableModel(readBookList(bl)).getModel());
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(BookController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;

        }        
    }
    private void writeBook(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getBook());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(BookController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    private Book readBook(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Book)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(bookFrame, e.getMessage(),bookFrame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(BookController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private void showBookForm(String selectedBook){
        bookFrame = selectedBook.isEmpty()?new BookForm():new BookForm(selectedBook);
        JDesktopPane desktop =  (JDesktopPane)bookList.getParent();
        desktop.add(bookFrame);
        bookFrame.setVisible(true);        
    }
    private Book[] readBookList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Book[] books;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
            books = (Book[]) s.readObject();
        }
        return books;
    }
}

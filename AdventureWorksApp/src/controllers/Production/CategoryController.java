/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers.Production;

import datalayer.SQLServerConnection;
import datalayer.DBCategory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import models.production.Category;
import models.tablemodel.CategoryTableModel;
import util.Result;
import views.Production.CategoryForm;
import views.Production.CategoryList;
import views.Report;
/**
 *
 * @author William Sanchez
 */
public class CategoryController implements ActionListener {
    CategoryForm categoryFrame;
    CategoryList categoryList;
    Category category;
    SQLServerConnection connection;
    public CategoryController(CategoryForm f, SQLServerConnection c){
        super();
        categoryFrame = f;
        connection = c;
    }
     public CategoryController(CategoryList l, SQLServerConnection c){
        super();
        categoryList = l;
        connection = c; 
    }   

    public Category getCategory(int CategoryID) {
        DBCategory db = new DBCategory(connection);
        
        try {
            category = db.getCategory(CategoryID);
        } catch (ClassNotFoundException|SQLException ex) {
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
            category = new Category();
        }
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
          
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "save":
                category = categoryFrame.getCategoryData();
                if(category.getProductCategoryID()==0){
                    save(category);
                }
                else
                {
                    update(category);
                }
                break;
            case "clear":
                categoryFrame.clear();
                break;
            case "new":
                showCategoryForm(0);
                break;
            case "show":
                showCategoryForm(categoryList.getSelectedCategory());
                break;
            case "showreport":
                showReportCategory();
                break;
            case "showreport2":
                showReport();
                break;            
            case "showProducts":
                showProductCatalog(categoryList.getSelectedCategory());
                break;
            case "justOneCategory":
                showJustOneCategory(categoryList.getSelectedCategory());
                break;
        }        
    }
    private void showCategoryForm(int selectedCategory){
        categoryFrame = selectedCategory==0?new CategoryForm(connection):new CategoryForm(connection,selectedCategory);
        JDesktopPane desktop =  (JDesktopPane)categoryList.getParent();
        desktop.add(categoryFrame);
        categoryFrame.setVisible(true);        
    }
    
    public TableModel getCagories (){
        CategoryTableModel ctm;
        DBCategory db = new DBCategory(connection);
        try {
            Category[] categories = db.getCategories();
            ctm = new CategoryTableModel(categories);
        } catch (ClassNotFoundException|SQLException ex) {
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
            ctm = new CategoryTableModel();
        }
        return ctm.getModel();
    }
    public void save(Category category){
        DBCategory db = new DBCategory(connection);
        try {
            Result r = db.insert(category);
            JOptionPane.showMessageDialog(this.categoryFrame,r.getMessage(),
            categoryFrame.getTitle(),(r.isOutcome()?JOptionPane.INFORMATION_MESSAGE:JOptionPane.WARNING_MESSAGE));
        } catch (ClassNotFoundException|SQLException ex) {
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(categoryFrame,ex.getMessage(),
                 categoryFrame.getTitle(),JOptionPane.WARNING_MESSAGE);            
        }
    }
    
    public void update(Category category){
        DBCategory db = new DBCategory(connection);
        try {
            Result r = db.update(category);
                    JOptionPane.showMessageDialog(this.categoryFrame,r.getMessage(),
                    categoryFrame.getTitle(),(r.isOutcome()?JOptionPane.INFORMATION_MESSAGE:JOptionPane.WARNING_MESSAGE));
        } catch (ClassNotFoundException|SQLException ex) {
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(categoryFrame,ex.getMessage(),
                 categoryFrame.getTitle(),JOptionPane.WARNING_MESSAGE);            
        }
    }
    
    public void showReport(){
        try{
            JDesktopPane desktop =  (JDesktopPane)categoryList.getParent();
            connection.Open(1);
            JInternalFrame wrpt = new Report("/resources/reports/Categories.jrxml",connection.getConnection(), "Listado de categorías").getReport();
            desktop.add(wrpt);
            wrpt.setVisible(true);
            desktop.getDesktopManager().maximizeFrame(wrpt);
        }
        catch(ClassNotFoundException | SQLException ex){
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, "CategoryController.showReport", ex);
        }
    }    

   
    public void showReportCategory(){
        try{ 
            connection.Open(1);
            new Report("/resources/reports/Categories.jrxml",connection.getConnection(),"Listado de categorías").showReport();            
        }
        catch(ClassNotFoundException | SQLException Ex){
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, "CategoryController.getReportCategory", Ex);      
        }
    }
     public void showProductCatalog(int CategoryID){
        try{
            JDesktopPane desktop =  (JDesktopPane)categoryList.getParent();
            connection.Open(1);
            Map parameters = new HashMap();
            parameters.put("ProductCategoryID",CategoryID);
            Report r = new Report("/resources/reports/ProductCatalogByCategory.jrxml", parameters, connection.getConnection());
            r.setTitle( "Catálogo de productos");
            JInternalFrame wrpt = r.getReport();
            desktop.add(wrpt );
            wrpt .setVisible(true);
            desktop.getDesktopManager().maximizeFrame(wrpt);
        }
        catch(ClassNotFoundException | SQLException ex){
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, "CategoryController.showReport", ex);
        }
    }  
     public void showJustOneCategory(int CategoryID){
        try{
            JDesktopPane desktop =  (JDesktopPane)categoryList.getParent();
            connection.Open(1);
            Map parameters = new HashMap();
            parameters.put("ProductCategoryID",CategoryID);
            parameters.put("Name","");
            Report r = new Report("/resources/reports/CategoriesWithSP.jrxml", parameters, connection.getConnection());
            r.setTitle( "Solo una categoría");
            JInternalFrame wrpt = r.getReport();
            desktop.add(wrpt );
            wrpt .setVisible(true);
            desktop.getDesktopManager().maximizeFrame(wrpt);
        }
        catch(ClassNotFoundException | SQLException ex){
            Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE, "CategoryController.showReport", ex);
        }
    }     
     
}

/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package views.misc;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import models.tablemodel.FileTableModel;

/**
 *
 * @author William Sanchez
 */
public class FileExplorerForm extends javax.swing.JInternalFrame {   
    /**
     * Creates new form FileExplorerForm
     */
    DefaultTreeModel treeModel;
    DefaultMutableTreeNode root;
    public FileExplorerForm() {
        initComponents();
        initTree();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainSpliPane = new javax.swing.JSplitPane();
        fileExplorerScrrollPane = new javax.swing.JScrollPane();
        fileExplorerTree = new javax.swing.JTree();
        fileListScrollPane = new javax.swing.JScrollPane();
        fileListTable = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/folders_explorer.png"))); // NOI18N

        mainSpliPane.setDividerLocation(250);

        fileExplorerTree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fileExplorerTreeMouseClicked(evt);
            }
        });
        fileExplorerScrrollPane.setViewportView(fileExplorerTree);

        mainSpliPane.setLeftComponent(fileExplorerScrrollPane);

        fileListScrollPane.setViewportView(fileListTable);

        mainSpliPane.setRightComponent(fileListScrollPane);

        getContentPane().add(mainSpliPane, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileExplorerTreeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fileExplorerTreeMouseClicked
        TreePath tp = fileExplorerTree.getPathForLocation(evt.getX(), evt.getY());        
        if (tp != null){
//            JOptionPane.showMessageDialog(this, String.valueOf(tp.getPathCount()) + " | " + tp.toString() + " | " + tp.getLastPathComponent().toString());
            File f = new File(tp.getLastPathComponent().toString());
            if(f.isDirectory()){
                fillFileListTable(f);
            }
        }                 
    }//GEN-LAST:event_fileExplorerTreeMouseClicked
    private void fillFileListTable(File f){
        try{
            FileTableModel model = new FileTableModel(f);
            fileListTable.setModel(model.getModel());           
        }
        catch(Exception ex){
            Logger.getLogger(FileExplorerForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected File[] getListFiles(String Path) {
        File file = new File(Path);
        return file.listFiles();
    }
/**

*/
    private void addChilds(DefaultMutableTreeNode rootNode, String path) {
        try{
            System.out.println(path);
            File[] files = this.getListFiles(path);
            for(File file:files) {
                if(file.isDirectory()) {
                    DefaultMutableTreeNode subDirectory = new DefaultMutableTreeNode(file, true);
                    subDirectory.setAllowsChildren(true);                   
//                    addChilds(subDirectory, file.getAbsolutePath());
                    rootNode.add(subDirectory);
                } 
//                else {
//                    rootNode.add(new DefaultMutableTreeNode(file.getName()));
//                }
            }
        }
        catch(Exception ex){
            Logger.getLogger(FileExplorerForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initTree() {

        root = new DefaultMutableTreeNode("Este equipo");
        File[] paths;
        FileSystemView fsv = FileSystemView.getFileSystemView();
        
        // Devuelve los nombres de ruta para los archivos y directorios
        paths = File.listRoots();
        // Para cada nombre de ruta en el conjunto de rutas
        for(File path:paths)
        {
            // Crea un nodo por cada unidad de almacenamiento encontrada
            DefaultMutableTreeNode d = new DefaultMutableTreeNode(path);   
            addChilds(d,path.getPath());
            root.add(d);
        }
        fileExplorerTree.removeAll();        
        treeModel = new DefaultTreeModel(root);
        fileExplorerTree.setModel(treeModel);
        treeModel.reload(root);
    }    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane fileExplorerScrrollPane;
    private javax.swing.JTree fileExplorerTree;
    private javax.swing.JScrollPane fileListScrollPane;
    private javax.swing.JTable fileListTable;
    private javax.swing.JSplitPane mainSpliPane;
    // End of variables declaration//GEN-END:variables
}

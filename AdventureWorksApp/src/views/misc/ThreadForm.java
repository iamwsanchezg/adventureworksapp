/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package views.misc;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

/**
 *
 * @author William Sanchez
 */
public class ThreadForm extends javax.swing.JInternalFrame {
    int noQueries = 60;
    /**
     * Creates new form ThreadForm
     */
    public ThreadForm() {
        initComponents();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        initButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        ouputTextArea = new javax.swing.JTextArea();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);

        initButton.setText("Iniciar");
        initButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initButtonActionPerformed(evt);
            }
        });

        ouputTextArea.setColumns(20);
        ouputTextArea.setRows(5);
        jScrollPane1.setViewportView(ouputTextArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(initButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 6, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(14, 14, 14))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(initButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void initButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initButtonActionPerformed
        Thread queryThread = new Thread() {
          public void run() {
            runQueries();            
          }
        };
        queryThread.start();
    }//GEN-LAST:event_initButtonActionPerformed
    
    private void runQueries() {
      for (int i = 0; i < noQueries; i++) {
          try {
//            runDatabaseQuery(i);
            Thread.sleep(1000);
            updateProgress(i);
          } catch (InterruptedException ex) {
              Logger.getLogger(ThreadForm.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
    }
    
    private void updateProgress(final int queryNo) {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          ouputTextArea.append(String.format("%s: %s%n","Query ",queryNo));
        }
      });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton initButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea ouputTextArea;
    // End of variables declaration//GEN-END:variables
}

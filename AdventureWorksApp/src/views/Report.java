/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package views;

/**
 *
 * @author William Sanchez
 */
import java.awt.Dimension;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
   
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.swing.JRViewer;
/**
 *
 * @author WILLIAM SÁNCHEZ
 */
public class Report extends Thread {
    private final String sourceFileName;
    private Map parametros;
    private final Connection con;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    /** Creates a new instance of Report
     * @param jrxmlSource
     * @param cx */ 
    public Report(String jrxmlSource,Connection cx, String windowTitle) {
        con=cx;
        sourceFileName=jrxmlSource;
        title = windowTitle;
    }
    public Report(String jrxmlSource,Map param,Connection cx) {
        con=cx;
        sourceFileName=jrxmlSource;
        parametros=param;
    }
    public Report(Map param,Connection cx) {
        con=cx;
        parametros=param;
        sourceFileName =null;
    } 
    
   public void showReport(){
        System.out.println(sourceFileName);
        System.out.println(parametros);
      try 
         { 
            InputStream source = getClass().getResourceAsStream(sourceFileName);
            JasperDesign jasperDesign = JRXmlLoader.load(source);           
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            if(parametros==null){
                parametros =new HashMap();
            }
            parametros.put("Logo",getClass().getResource("/resources/images/logo.png").toString());
            System.out.println("Llenando reporte");
            JasperPrint report = JasperFillManager.fillReport(jasperReport,parametros, con);
            System.out.println("Report completo");
            JasperViewer.viewReport(report, false);        
         }
         catch (JRException e) 
         {           
               Logger.getLogger(Report.class.getName()).log(Level.SEVERE, "Report.showReport", e);
         } 
    }
    public JInternalFrame getReport(){
        System.out.println(sourceFileName);
        System.out.println(parametros);
        JInternalFrame jif=null;
      try 
         { 
            InputStream source = getClass().getResourceAsStream(sourceFileName);
            JasperDesign jasperDesign = JRXmlLoader.load(source);           
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            if(parametros==null){
                parametros =new HashMap();
            }
            parametros.put("Logo",getClass().getResource("/resources/images/logo.png").toString());
            System.out.println("Llenando reporte");
            JasperPrint report = JasperFillManager.fillReport(jasperReport,parametros, con);
            System.out.println("Report completo");
            JRViewer jrv = new JRViewer(report);
            jrv.setPreferredSize(new Dimension(600, 410));
            JScrollPane reportScroll = new JScrollPane(jrv);
            jif=new JInternalFrame(title,true,true,true,true);
            jif.setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/report.png")));
            jif.setSize(800,600);
            jif.add(reportScroll);          
         }
         catch (JRException e) 
         {           
               Logger.getLogger(Report.class.getName()).log(Level.SEVERE, "Report.getReport", e);
         }  
        return jif;
    } 
   public JInternalFrame getReportByResultSet(ResultSet resultSet){
        System.out.println(parametros);
        JInternalFrame jif=null;
      try 
         { 
        InputStream source = getClass().getResourceAsStream(sourceFileName);
        JasperDesign jasperDesign = JRXmlLoader.load(source);           
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        if(parametros==null){
             parametros =new HashMap();
         }
          parametros.put("Logo",getClass().getResource("/resources/images/logo.png").toString());
          System.out.println("Llenando reporte");
          JasperPrint report = JasperFillManager.fillReport(jasperReport,parametros, new JRResultSetDataSource(resultSet));
          System.out.println("Report completo");
          JasperViewer jrv = new JasperViewer(report);
          jrv.setPreferredSize(new Dimension(600, 410));
          JScrollPane reportScroll = new JScrollPane(jrv);
          jif=new JInternalFrame(title,true,true,true,true);
          jif.setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/report.png")));
          jif.setSize(800,600);
          jif.add(reportScroll);
          
         }
         catch (JRException e) 
         {           
               Logger.getLogger(Report.class.getName()).log(Level.SEVERE, "Report.getReportByResultSet", e);
         }  
        return jif;
    }
}

/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package views.Production;

import datalayer.SQLServerConnection;

/**
 *
 * @author PC
 */
public class ProductForm extends javax.swing.JInternalFrame {
   
    
    /**
     * Creates new form FrmProductManager
     */
    public ProductForm() {
        initComponents();
    }

    public ProductForm(SQLServerConnection connection, int productID) {
        initComponents();
    }
    


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbMoreData = new javax.swing.JTabbedPane();
        pInventory = new javax.swing.JPanel();
        inventoryScp = new javax.swing.JScrollPane();
        inventoryGrid = new javax.swing.JTable();
        pCostHistory = new javax.swing.JPanel();
        costHistoryScp = new javax.swing.JScrollPane();
        costHistoryGrid = new javax.swing.JTable();
        pListPriceHistory = new javax.swing.JPanel();
        listPriceHistoryScp = new javax.swing.JScrollPane();
        listPriceHistoryGrid = new javax.swing.JTable();
        pPhotos = new javax.swing.JPanel();
        photosScp = new javax.swing.JScrollPane();
        photoGrid = new javax.swing.JTable();
        pDocument = new javax.swing.JPanel();
        documentScp = new javax.swing.JScrollPane();
        documentGrid = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Producto");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons/PRODUCT.PNG"))); // NOI18N
        getContentPane().setLayout(null);

        pInventory.setLayout(new java.awt.GridLayout(1, 0));

        inventoryGrid.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        inventoryScp.setViewportView(inventoryGrid);

        pInventory.add(inventoryScp);

        tbMoreData.addTab("Inventario", pInventory);

        pCostHistory.setLayout(new java.awt.GridLayout(1, 0));

        costHistoryScp.setViewportView(costHistoryGrid);

        pCostHistory.add(costHistoryScp);

        tbMoreData.addTab("Historial de costos", pCostHistory);

        pListPriceHistory.setLayout(new java.awt.GridLayout(1, 0));

        listPriceHistoryScp.setViewportView(listPriceHistoryGrid);

        pListPriceHistory.add(listPriceHistoryScp);

        tbMoreData.addTab("Historial de precios de lista", pListPriceHistory);

        pPhotos.setLayout(new java.awt.GridLayout(1, 0));

        photoGrid.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        photosScp.setViewportView(photoGrid);

        pPhotos.add(photosScp);

        tbMoreData.addTab("Fotos", pPhotos);

        pDocument.setLayout(new java.awt.GridLayout(1, 0));

        documentScp.setViewportView(documentGrid);

        pDocument.add(documentScp);

        tbMoreData.addTab("Documentos", pDocument);

        getContentPane().add(tbMoreData);
        tbMoreData.setBounds(0, 90, 455, 410);
        tbMoreData.getAccessibleContext().setAccessibleName("Inventory");

        jLabel1.setText("jLabel1");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 41, 16);

        jTextField1.setText("jTextField1");
        getContentPane().add(jTextField1);
        jTextField1.setBounds(50, 0, 75, 24);

        jLabel2.setText("jLabel2");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 30, 41, 16);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable costHistoryGrid;
    private javax.swing.JScrollPane costHistoryScp;
    private javax.swing.JTable documentGrid;
    private javax.swing.JScrollPane documentScp;
    private javax.swing.JTable inventoryGrid;
    private javax.swing.JScrollPane inventoryScp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTable listPriceHistoryGrid;
    private javax.swing.JScrollPane listPriceHistoryScp;
    private javax.swing.JPanel pCostHistory;
    private javax.swing.JPanel pDocument;
    private javax.swing.JPanel pInventory;
    private javax.swing.JPanel pListPriceHistory;
    private javax.swing.JPanel pPhotos;
    private javax.swing.JTable photoGrid;
    private javax.swing.JScrollPane photosScp;
    private javax.swing.JTabbedPane tbMoreData;
    // End of variables declaration//GEN-END:variables
}

/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package util;

/**
 *
 * @author William Sanchez
 */
public class Result {
    private boolean Outcome;
    private String Message;
    private int Id;

    public Result() {
        Outcome=false;
        Message="";
        Id=0;
    }

    public Result(boolean Outcome, String Message, int Id) {
        this.Outcome = Outcome;
        this.Message = Message;
        this.Id = Id;
    }

    public boolean isOutcome() {
        return Outcome;
    }

    public void setOutcome(boolean Outcome) {
        this.Outcome = Outcome;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
    
}

/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package datalayer;

import models.production.Category;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import util.Result;

/**
 *
 * @author William Sanchez
 */
public class DBCategory {
    private SQLServerConnection dbConn;
    private Category category;
    public DBCategory(){
        super();
    }
    public DBCategory(SQLServerConnection dbConn){
        this.dbConn = dbConn;
    }

    public DBCategory(SQLServerConnection dbConn, Category category) {
        this.dbConn = dbConn;
        this.category = category;
    }

    public SQLServerConnection getDbConn() {
        return dbConn;
    }

    public void setDbConn(SQLServerConnection dbConn) {
        this.dbConn = dbConn;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    public Category[] getCategories() throws SQLException, ClassNotFoundException{
        Category [] categories;
        try{
            dbConn.Open(1);
            Statement Command = dbConn.connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = Command.executeQuery("EXEC Production.uspGetCategories");
            categories = new Category[Util.getRowCount(rs)];
            int index =0;
            while(rs.next()){
                Category c = new Category();
                c.setProductCategoryID(rs.getInt("ProductCategoryID"));
                c.setName(rs.getString("Name"));
                c.setRowguid(rs.getString("rowguid"));
                c.setModifiedDate(rs.getDate("ModifiedDate"));
                
                categories[index] = c;
                index++;
            }
            dbConn.CloseConnection();
        }
        catch(SQLException e){
            throw e;
        }
        return categories;
    }
    
    public Category getCategory(int CategoryID) throws SQLException, ClassNotFoundException{
        Category c = new Category();

        try{
            dbConn.Open(1);
            CallableStatement Command = dbConn.connection.prepareCall("{call Production.uspGetCategories(?)}");
            Command.setInt("ProductCategoryID", CategoryID);
            ResultSet rs = Command.executeQuery();
            if(rs.next()){
                c.setProductCategoryID(rs.getInt("ProductCategoryID"));
                c.setName(rs.getString("Name"));
                c.setRowguid(rs.getString("rowguid"));
                c.setModifiedDate(rs.getDate("ModifiedDate"));
            }
            else{
                throw new SQLException("No se encontró una categoría con el ID indicado");
            }
            dbConn.CloseConnection();
        }
        catch(SQLException e){
            throw e;
        }
        return c;
    }

    public Category getCategory(int CategoryID, String CategoryName) throws SQLException{
        Category c = new Category();
        try{
            dbConn.Connect();
            CallableStatement Command = dbConn.connection.prepareCall("{call Production.uspGetCategories(?,?)}");
            Command.setInt("ProductCategoryID", CategoryID);
            Command.setString("Name", CategoryName);
            ResultSet rs = Command.executeQuery();
            if(rs.next()){
                c.setProductCategoryID(rs.getInt("ProductCategoryID"));
                c.setName(rs.getString("Name"));
                c.setRowguid(rs.getString("rowguid"));
                c.setModifiedDate(rs.getDate("ModifiedDate"));
            }
            else{
                throw new SQLException("No se encontró una categoría con los parámetros indicados indicado");
            }          
        }
        catch(SQLException e){
            throw e;
        }
        return c;
    }
    public Result insert(Category category)throws ClassNotFoundException, SQLException {
        Result r=new Result();
        try
        {  
            dbConn.Open(1);
            CallableStatement Command=dbConn.connection.prepareCall("{call Production.usp_AddProductCategory(?,?,?,?)}");
            Command.setString("Name",category.getName());
            Command.registerOutParameter("ProductCategoryID",Types.INTEGER);
            Command.registerOutParameter("RETURN",Types.TINYINT);
            Command.registerOutParameter("RETURN_MESSAGE",Types.NVARCHAR);
            Command.execute();
            r.setId(Command.getInt("ProductCategoryID"));
            r.setMessage(Command.getString("RETURN_MESSAGE"));
            r.setOutcome((Command.getInt("RETURN")==0));
        }
        catch(SQLException e)
        {  
           throw e;
        }
        return r;
    }
    public Result update(Category category)throws ClassNotFoundException, SQLException {
        Result r=new Result();
        try
        {  
            dbConn.Open(1);
            CallableStatement Command=dbConn.connection.prepareCall("{call Production.usp_UpdateProductCategory(?,?,?,?)}");
            Command.setInt("ProductCategoryID", category.getProductCategoryID());
            Command.setString("Name",category.getName());            
            Command.registerOutParameter("RETURN",Types.TINYINT);
            Command.registerOutParameter("RETURN_MESSAGE",Types.NVARCHAR);
            Command.execute();
            r.setMessage(Command.getString("RETURN_MESSAGE"));
            r.setOutcome((Command.getInt("RETURN")==0));
        }
        catch(SQLException e)
        {  
           throw e;
        }
        return r;
    }
}
